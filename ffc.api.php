<?php

/**
 * @file
 * Hooks provided by Field formatter conditions.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implements hook_ffc_conditions_info().
 */
function hook_ffc_conditions_info() {
  $conditions = array();

  $conditions['show'] = array(
    'title' => t('Hide field when target is not empty'),
    'callback' => 'ffc_hide_when_target_is_not_empty',
  );

  return $conditions;
}

/**
 * @} End of "addtogroup hooks".
 */